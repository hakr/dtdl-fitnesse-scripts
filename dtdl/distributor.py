from dtdl.soap import *
from datetime import *

def timestamp():
        '''Function to create datetime object to string.'''
        dt_obj = datetime.now()
        date_str = dt_obj.strftime("%Y%m%d%H%M%S")
        return date_str

class SoapResponder:
  def __init__(self):
    self.response = '???'

  def soapResponse(self):
    return self.response

class DistributeurOrderSimulator(SoapResponder):

  def __init__(self,host):
    SoapResponder.__init__(self)
    self.service = OrderSystemService(host)

  def PlaatsOrderAlsDistributeurVoorArtikelMetAantal(self, distributeur_id, ean, hoeveelheid ):
    self.response = self.service.placeOrder( distributeur_id, ean, hoeveelheid, timestamp(), timestamp() + 'MLMB' )

class DistributeurSpecificeerSimulator(SoapResponder):

  def __init__(self,host,distributeur_id,distributeur_wachtwoord):
    SoapResponder.__init__(self)
    self.service = SpecificationService(host, distributeur_id, distributeur_wachtwoord)

  def SpecificeerIBFKrediet(self, EAN, Amount, StartDate, ContactName, ContactEmail, DistributorCreditId, DistributorSchoolId ):
    start_date = datetime.strptime(StartDate,'%d-%m-%Y').date()
    self.response = self.service.SpecifyIBFCredit( EAN, Amount, start_date, ContactName, ContactEmail, DistributorCreditId, DistributorSchoolId )

