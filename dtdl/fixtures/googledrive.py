from google_spreadsheet.api import SpreadsheetAPI

class GoogleSpreadsheet:

    def __init__(self, username, password):
        self.api = SpreadsheetAPI(username, password, 'DTDL_TEST')
        self.spreadsheets = self.api.list_spreadsheets()
        print "%d spreadsheets" % len(self.spreadsheets)

    def get_sheet_names(self, docname):
        dockey = self._find_in_list(self.spreadsheets,docname)

        if dockey:
            sheets = self.api.list_worksheets(dockey)
            result = []
            for sheet in sheets:
                result.append(sheet[0])
            return result
        else:
            return []


    def get_sheet(self,docname,sheetname):
        dockey = self._find_in_list(self.spreadsheets,docname)

        if dockey:
            sheets = self.api.list_worksheets(dockey)
            print "Sheets: %r" % (sheets)
            sheetkey = self._find_in_list(sheets,sheetname)
            if sheetkey:
                return self.api.get_worksheet(dockey,sheetkey)


    def _find_in_list(self,list, key):
        for item in list:
            if key in item:
                return item[1]
        return None
