from dtdl.models import *
import datetime
import re, os

NONIDENTIFIER = re.compile('\s|\&|\*|\&|[+-.:;!@]')

class magic_property(object):
    def __init__(self, get_func):

        self.get_func = get_func

    def __get__(self, instance, owner):
        if instance is None:
            return self
        instance.attribute_name = self.get_func.__name__
        return self.get_func(instance)


class ObjectDefinition(object):
    def __init__(self,definitions,class_name,name,attributes):
        self.definitions = definitions
        self.attributes = attributes
        self.name = name
        self.class_name = class_name
        self.attribute_name = ''

        # rewrite the attributes to sanitized form
        attributes_original = attributes.copy()
        self.attributes.clear()

        for attribute_name in attributes_original.keys():
            self.attributes[self.sanitize_name(attribute_name)] = attributes_original[attribute_name]

        # self.definitions.define(self.class_name, self.name,self.attributes)

    def __nonzero__(self):
        return len(self.attributes)>0


    def sanitize_name(self,value):
        result = re.sub(NONIDENTIFIER, '_', value).lower()
        return result

    def magic_value(self,matcher,magicfn):
        attribute_name = self.attribute_name
        value = self.get_definition(attribute_name)
        if matcher.match(value):
            value = magicfn(value)
            self.set_attribute(attribute_name, value)
            self.definitions.register_magic_value(self,attribute_name,value)
        return value


    def set_attribute(self,attr_name,attr_value):
        self.attributes[attr_name] = attr_value
        # self.definitions.define_attribute(self.class_name,self.name,attr_name,attr_value)


    def get_definition(self,def_name):
        name = self.sanitize_name(def_name)
        if name in self.attributes:
            attr_value = self.attributes[name]

            model = self.definitions.get_definition(name,attr_value)
            if model:
                return model
            else:
                return attr_value
        else:
            return None

    def __str__(self):
        if self.attributes:
            return "%r" % self.attributes
        else:
            return "<<NO ATTRIBUTES>"

    def __getattr__(self,name):
        def _get_definition(*args):
            return self.get_definition(name)

        self.attribute_name = name
        return _get_definition()

def create_ean():
    today = datetime.datetime.today()
    return "%04d%02d%02d%05d" % (today.year, today.month, today.day, today.hour*3600 + today.minute*60 + today.second)

def create_mban():
    nu = datetime.datetime.now()
    fname = 'mban.dat'
    sequence_number = 0
    if os.path.isfile(fname):
        f = open(fname,'r')
        try:
            sequence_number = int(f.read())
        except ValueError:
            sequence_number = 0
        f.close()
    sequence_number += 1

    f=open(fname,'w')
    f.write( '%d' % sequence_number )
    f.close()

    return '%06d' % ((nu.timetuple()[7] + 633) * 1000 + sequence_number)

def timestamp():
        '''Function to create datetime object to string.'''
        dt_obj = datetime.datetime.now()
        date_str = dt_obj.strftime("%Y%m%d%H%M%S")
        return date_str

def get_jaartal(offset=0):
    return datetime.datetime.now().year + offset

def dag_maand(dm):
    # start_date = datetime.strptime('%s-%d' % (dm,self.get_jaartal()),'%d-%m-%Y').date()
    print "DAG en MAAND = %s" % dm
    return "%s-%4d" % (dm,get_jaartal())

def create_credit_id():
    return "S-CR-%s" % timestamp()

AUTODEF = re.compile('auto.*')
DATEREF = re.compile('vandaag')
DATEPREVIOUSREF = re.compile('vorig')
DATEDAYMONTHONLY = re.compile('\d{1,2}-\d{1,2}$')

class Models:

    class ArtikelDefinition(ObjectDefinition):
        @magic_property
        def ean(self):
            return self.magic_value(AUTODEF, (lambda x: create_ean()) )

        @magic_property
        def mban(self):
            return self.magic_value(AUTODEF, (lambda x: create_mban()) )

        @magic_property
        def jaar(self):
            return self.magic_value(DATEREF, (lambda x: get_jaartal(0)) )


    class SpecificatieDefinition(ObjectDefinition):
        @magic_property
        def start_datum(self):
            return self.magic_value(DATEDAYMONTHONLY, (lambda x: dag_maand(x)) )

        @magic_property
        def distributeur_credit_id(self):
            return self.magic_value(AUTODEF, (lambda x: create_credit_id()) )

    class OrderDefinition(ObjectDefinition):
        @magic_property
        def referentie(self):
            return self.magic_value( AUTODEF, (lambda x: timestamp()))

        @magic_property
        def malmberg_referentie(self):
            return self.magic_value( AUTODEF, (lambda x: timestamp() + 'MB'))
        # @property
        # def artikel(self):
        #     artikel_definition_name = 'Artikel|' + self.get_definition('artikel')
        #     return self.definitions.get_definition(artikel_definition_name)

        # @property
        # def distributeur(self):
        #     order_definition_name = 'Order|' + self.get_definition('order')
        #     return self.definitions.get_definition(order_definition_name)



class ObjectDefinitions:
    ''' named definitions '''


    def __init__(self):
        self.definitions = {}
        self.magic_values = {}

    def dump(self):
        for def_class in self.definitions.keys():
            print "CLASS %s" % def_class
            for def_name in self.definitions[def_class].keys():
                print "   %s: %r." % (def_name, self.definitions[def_class][def_name])


    def sanitize_name(self,value):
        result = re.sub(NONIDENTIFIER, '_', value).capitalize()
        return result.lower()

    def sanitize_class_name(self,value):
        result = re.sub(NONIDENTIFIER, '_', value).capitalize()
        return result[0].capitalize() + result[1:].lower()

    def define(self,def_class,def_name,def_value):
        class_name = self.sanitize_class_name(def_class)
        if not class_name in self.definitions:
            self.definitions[class_name] = {}

        self.definitions[class_name][self.sanitize_name(def_name)] = def_value

    def define_attribute(self,def_class, def_name,def_attr,def_value):

        attr_name = def_attr
        self.definitions[self.sanitize_class_name(def_class)][self.sanitize_name(def_name)][attr_name] = def_value

    def register_magic_value(self,definition, def_attr, attr_value):
        def_class = definition.class_name[:-10]
        if not def_class in self.magic_values:
            self.magic_values[def_class] = {}

        def_name = definition.name
        if not def_name in self.magic_values[def_class]:
            self.magic_values[def_class][def_name] = []

        self.magic_values[def_class][def_name].append( {'name': def_attr, 'new_value': attr_value} )


    def get_definition(self,def_class,def_name):
        class_name = self.sanitize_class_name(def_class)

        if class_name in self.definitions:

            name = self.sanitize_name(def_name)

            if name in self.definitions[class_name]:

                object_name = class_name + 'Definition'

                if hasattr(Models, object_name):
                    return getattr(Models, object_name)(self,object_name,name,self.definitions[class_name][name])
                else:
                    return ObjectDefinition(self,object_name,name,self.definitions[class_name][name])
            else:
                return None
        else:
            return None


