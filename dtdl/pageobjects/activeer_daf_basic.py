# selenium webdriver
from selenium import webdriver
from selenium.webdriver.common.by import By

import base

class SimuleerDAFUser(base.PageObject):


    _invullen_locator = (By.ID, "daf_leerling_ibf")
    _EAN_edit_locator = (By.ID, "ean")
    _Email_edit_locator = (By.ID, "email")
    _firstname_edit_locator = (By.ID, "firstName")
    _secondname_edit_locator = (By.ID, 'lastNamePrefix')
    _lastname_edit_locator = (By.ID, "lastName")
    _pid_edit_locator = (By.ID, "portalId")
    _pui_edit_locator = (By.ID, "portalUserId")
    _submit_locator = (By.CSS_SELECTOR, "input[type=\"submit\"]")
    _generated_link_locator = (By.ID, "dafLink")


    def definieerGebruiker(self, artikel, gebruiker ):

        self.driver.find_element(*self._invullen_locator).click()

        self.set_text_value( self._EAN_edit_locator, artikel.ean)
        self.set_text_value( self._Email_edit_locator, gebruiker.e_mail)
        self.set_text_value( self._firstname_edit_locator, gebruiker.voornaam)
        self.set_text_value( self._secondname_edit_locator, gebruiker.tussenvoegsels)
        self.set_text_value( self._lastname_edit_locator, gebruiker.achternaam)
        self.set_text_value( self._pid_edit_locator, gebruiker.portal.portal_id)
        self.set_text_value( self._pui_edit_locator, gebruiker.portal_user_id)

        self.driver.find_element(*self._submit_locator).click()


    def getGeneratedLink(self):
      link_node = self.driver.find_element(*self._generated_link_locator)
      if link_node:
        result = link_node.get_attribute('href')
      else:
        result = None

      return result



