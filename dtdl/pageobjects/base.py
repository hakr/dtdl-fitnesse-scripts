from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

class PageObject:
    ''' Abstract base class for all page objects '''

    def __init__(self,driver):
        self.driver = driver

    def set_text_value(self,locator,value):
        element = self.find_element(locator)

        if not element is None:
            element.clear()
            if value.__class__ is str:
              if len(value)>0:
                element.send_keys(value)
            else:
              element.send_keys(value)

    def find_element(self,locator):
        try:
            element = self.driver.find_element(*locator)
        except NoSuchElementException:
            return None
        return element

    def find_elements(self,locator):
        try:
            elements = self.driver.find_elements(*locator)
        except NoSuchElementException:
            return []
        return elements
