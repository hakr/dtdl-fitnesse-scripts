from selenium import webdriver
from selenium.webdriver.common.by import By

import base


_username_locator = (By.NAME, "username")
_password_locator = (By.NAME, "password")

_submit_locator   = (By.NAME, 'submit')
_error_locator    = (By.CLASS_NAME, "feedbackPanelERROR" )

class InloggenPage(base.PageObject):



    def invullenAccount(self, admin_name, admin_password):

        self.set_text_value(_username_locator, admin_name)
        self.set_text_value(_password_locator, admin_password)

    def checkAccount(self):
        self.driver.find_element(*_submit_locator).click()

        try:
            errorbox = self.driver.find_element(*_error_locator)
        except:
            errorbox = None

        print "ERRORBOX after logon = %r" % errorbox

        return errorbox is None
