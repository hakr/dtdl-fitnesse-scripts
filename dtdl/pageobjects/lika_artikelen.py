from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select

import base




class ArtikelenPage(base.PageObject):
    _EAN_locator = (By.NAME, 'ean')

    _zoek_button_locator = (By.CSS_SELECTOR, 'input[type="submit"][value="Zoek"]')
    _no_records_locator = (By.CLASS_NAME, "norecords-tr")


    def gaToevoegen(self):
        self.find_element((By.ID,"articleCreationLink")).click()
        return ArtikelToevoegenPage(self.driver)

    def zoekArtikel2(self,artikel):
        self.set_text_value(self._EAN_locator, artikel.ean)

    def zoekArtikel(self,artikel):
        self.set_text_value(self._EAN_locator, artikel.ean)

        self.find_element(self._zoek_button_locator).click()

        not_found_indicator = self.find_element(self._no_records_locator)

        return not_found_indicator is None

    def kiesArtikel(self,artikel):
        self.find_element((By.LINK_TEXT, artikel.ean)).click()

        return ArtikelDetailPage(self.driver)




class ArtikelDetailPage(base.PageObject):
    _nieuwe_koppeling_locator = (By.LINK_TEXT,"Nieuwe koppeling toevoegen")
    _next_button_locator = (By.NAME, 'nextButton')
    _app_name_locator = (By.NAME, 'wizardPanel:stepPanel:details:editForm:educationalApplication')
    _role_locator = (By.NAME, 'wizardPanel:stepPanel:details:editForm:role')


    def koppelArtikelAanApplicatie(self,artikel,applicatie_items):

        self.find_element(self._nieuwe_koppeling_locator).click()

        app = Select(self.find_element(self._app_name_locator))
        app.select_by_visible_text(applicatie_items.applicatie_naam)


    def koppelArtikelAanRol(self,artikel,applicatie_items):

        self.find_element(self._next_button_locator).click()

        app = Select(self.find_element(self._role_locator))
        app.select_by_visible_text(applicatie_items.rol)


    def koppelArtikelAanItems(self,artikel,applicatie_items):

        self.find_element(self._next_button_locator).click()

        items = applicatie_items.items.split(';')

        search = []
        for item in items:
            search.append(item.strip().lower())

        nodes = self.find_elements((By.CLASS_NAME, 'tree-node'))

        for node in nodes:

            label = node.find_element(By.CLASS_NAME, 'tree-folder-other')

            if label.text.lower() in search:

                checkbox = node.find_element(By.CSS_SELECTOR, 'input')
                checkbox.click()


        return True

    def opslaan(self):
        self.find_element(self._next_button_locator).click()

        return not self.find_element((By.CLASS_NAME, 'feedbackPanelINFO')) is None

class ArtikelToevoegenPage(base.PageObject):
    _EAN_locator = (By.NAME, 'article.ean')
    _MBAN_locator = (By.NAME, 'article.mban')
    _omschrijving_locator = (By.NAME, 'article.title2')
    _merk_locator = (By.NAME, 'article.brand')
    _methode_locator = (By.NAME, 'article.methodName')
    _afbeelding_locator = (By.NAME, 'article.thumb')
    _ean_url_locator = (By.NAME, 'article.url')
    _vak_locator = (By.NAME, 'article.subject')
    _niveau_locator = (By.NAME, 'article.stream')
    _jaar_locator = (By.NAME, 'article.year')
    _duur_locator = (By.NAME, 'article.durationString')
    _start_datum_restrictie_locator = (By.NAME, 'article.startDateRestriction')
    _eula_locator = (By.NAME, 'article.eula')

    _onderwijstype_locator = (By.NAME, 'article.context')
    _licentietype_locator = (By.NAME, 'article.licenseType')

    _rol_locator = (By.XPATH, '//input[@type="radio"][@name="article.role"]/../label')

    _OK_locator = (By.CSS_SELECTOR, '.buttons input[value=Toevoegen]')

    _success_locator = (By.CSS_SELECTOR, "li.feedbackPanelINFO")

    def invullen(self,artikel):
        self.set_text_value(self._EAN_locator, artikel.ean)
        self.set_text_value(self._MBAN_locator, artikel.mban)
        self.set_text_value(self._omschrijving_locator, artikel.omschrijving)
        self.set_text_value(self._merk_locator, artikel.merk)
        self.set_text_value(self._methode_locator, artikel.methode)
        self.set_text_value(self._afbeelding_locator, artikel.afbeelding_url)
        self.set_text_value(self._ean_url_locator, artikel.ean_url)
        self.set_text_value(self._vak_locator, artikel.vak)
        self.set_text_value(self._niveau_locator, artikel.niveau)
        self.set_text_value(self._jaar_locator, artikel.jaar)
        self.set_text_value(self._duur_locator, artikel.duur)
        self.set_text_value(self._start_datum_restrictie_locator, artikel.start_datum_restrictie)
        self.set_text_value(self._eula_locator, artikel.eula)


        owt = Select(self.find_element(self._onderwijstype_locator))
        owt.select_by_visible_text(artikel.onderwijstype)

        lt = Select(self.find_element(self._licentietype_locator))
        lt.select_by_visible_text(artikel.licentietype)

        labels = self.find_elements(self._rol_locator)

        selected_label = None
        for label in labels:
            if label.text == artikel.rol:
                selected_label = label
                print selected_label
                print selected_label.get_attribute('for')

        if selected_label:
            rol_node = self.find_element((By.ID, selected_label.get_attribute('for')))
            rol_node.click()

    def opslaan(self):

        self.find_element(self._OK_locator).click()

        result = False
        try:
            result_node = self.find_element(self._success_locator)
            if result_node:
                result = True
        except:
            result = False

        return result
        # self.assertEqual("Artikel aangemaakt voor EAN: 2013062600101", .text)

