from selenium.webdriver.common.by import By
from inloggen import *
from activeer_daf_basic import *
from lika_artikelen import *
import time
import base

class NavigatorPage:
    def __init__(self,driver,host):
        self.driver = driver
        self.host = host
        self.base_url = ''

    def open(self,path=''):
        print "OPEN(%s,%s)" % (self.base_url,path)
        if path == '':
            self.driver.get(self.base_url)
        else:
            self.driver.get(self.base_url + path)

class Lika(NavigatorPage):
    ''' main navigator for the license office pages 'Licentiekantoor' '''

    def __init__(self,driver,host):
        NavigatorPage.__init__(self,driver,host)

        self.base_url = host + '/backend/'

    def openAanmelden(self):
        self.open()
        return InloggenPage(self.driver)

    def openArtikelen(self):
        self.open('artikelen')
        # IDs no longer available on ACC
        # self.driver.find_element_by_id("Catalogus").click()
        # self.driver.find_element_by_id("Artikelen").click()

        return ArtikelenPage(self.driver)




class DummyStartPage(NavigatorPage):
    ''' navigator for the dummy start page '''

    _DAF_link_locator = (By.LINK_TEXT, "DAF")


    def __init__(self,driver,host):
        NavigatorPage.__init__(self,driver,host)

        # self.base_url = host + '/dummy-startpage/'
        self.base_url = 'http://tst.toegang.malmberg.nl/acc-startpage'

    def openDAFBasic(self):
        self.driver.get('http://tst.toegang.malmberg.nl')
        self.driver.get('http://tst.toegang.malmberg.nl/acc-startpage')

        time.sleep(2)

        try:
            link = self.driver.find_element(*self._DAF_link_locator)
            link.click()
        except:
            return None


        return SimuleerDAFUser(self.driver)

    def followGeneratedLink(self,url):
        self.driver.get(url)

        tester = base.PageObject(self.driver)
        return tester.find_element((By.CSS_SELECTOR, 'error')) is None
