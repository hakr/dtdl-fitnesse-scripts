from random import *
from datetime import *

# to make soap calls
from suds.client import Client
from suds.sax.element import Element
from suds.sax.attribute import Attribute
from suds.xsd.sxbasic import Import
import suds
import urllib2
import logging
import random
import contextlib
import urllib2 as u2
from suds.transport.http import HttpTransport, Reply, TransportError
import httplib
import os

def xs_date(adate = None):
    '''Function to convert data object to xs:date string.'''
    if adate:
        pass
    else:
        adate = datetime.now()

    if adate.__class__ is date:
        date_str = adate.strftime("%Y-%m-%d")
    elif adate.__class__ is str or adate.__class__ is unicode:
        date_str = datetime.strptime(adate,'%d-%m-%Y').date().strftime("%Y-%m-%d")
    else:
        date_str = str(adate)
    return date_str

def join_url_parts(tuple):
    result = tuple[0]
    for t in tuple[1:]:
        if result.endswith('/'):
            result += t
        else:
            result += '/' + t
    return result


class HTTPSClientAuthHandler(u2.HTTPSHandler):
    def __init__(self, key, cert):
        u2.HTTPSHandler.__init__(self)
        self.key = key
        self.cert = cert

    def https_open(self, req):
        #Rather than pass in a reference to a connection class, we pass in
        # a reference to a function which, for all intents and purposes,
        # will behave as a constructor
        return self.do_open(self.getConnection, req)

    def getConnection(self, host, timeout=300):
        return httplib.HTTPSConnection(host, key_file=self.key, cert_file=self.cert)

class HTTPSClientCertTransport(HttpTransport):
    def __init__(self, key, cert, *args, **kwargs):
        HttpTransport.__init__(self, *args, **kwargs)
        self.key = key
        self.cert = cert

    def u2open(self, u2request):
        """
        Open a connection.
        @param u2request: A urllib2 request.
        @type u2request: urllib2.Requet.
        @return: The opened file-like urllib2 object.
        @rtype: fp
        """
        tm = self.options.timeout
        url = u2.build_opener(HTTPSClientAuthHandler(self.key, self.cert))
        print "u2open(%r,%r), %r" % (url, u2request,self.key)
        print dir(url)
        if self.u2ver() < 2.6:
            socket.setdefaulttimeout(tm)
            return url.open(u2request)
        else:
            return url.open(u2request, timeout=tm)

# These lines enable debug logging; remove them once everything works.
# import logging
# logging.basicConfig(level=logging.INFO)
logging.getLogger('suds.client').setLevel(logging.DEBUG)
logging.getLogger('suds.transport').setLevel(logging.DEBUG)

class SoapService:
    def __init__(self,wsdl_url):
        if wsdl_url[0:5] == 'http:':
            wsdl_url = 'https' + wsdl_url[4:]
        pemfile = 'dtdl/certificates/lpp.secure.acc.toegang.malmberg.nl.pem'
        print "SOAP client on %s, from %s" % (wsdl_url, os.getcwd())
        self._soap = Client(wsdl_url, faults=False) # , transport = HTTPSClientCertTransport(pemfile, pemfile)
        # self._soap.options.faults = False # errors are reported with HTTP status 500

    def invoke(self, service, parameters):
        print "invoke " + `service` + " with " + `parameters`
        for header in service.method.soap.input.headers:
            element_qname = header.part.element
            element_designation = element_qname[0]
            if element_designation == 'AuthenticationHeader':
                headernode = self._soap.factory.create("{%s}%s" % (element_qname[1], element_designation))
                headernode.DistributorId = self.distributorid
                headernode.Password = self.distributorpassword
                self._soap.set_options(soapheaders=(headernode))
            else:
                raise "Empty header! ", headernode

        result ='?'
        response = ()
        try:
            response = service(*parameters)
            print "response=" + `response`
            if response[0] == 500:
                result = ''
                details = response[1][0]
                if 'Code' in details:
                    result = "error %s: " % details['Code']
                if 'Message' in details:
                    result += details['Message']
                if 'message' in details:
                    result += details['message']
            else:
                result = `response`
        except Exception, e:
            print "AIAIAI! " + `e`
            result = "%s - %r" % (str(e), response)

        return result


class OrderSystemService(SoapService):
    def __init__(self,host):

        SoapService.__init__(self, join_url_parts((host,'backend/ws/OrderSystemService?wsdl')) )

    def placeOrder(self, distrid, ean, quantity, order_reference, malmberg_reference):
        return self.invoke(self._soap.service.RegisterOrder, (distrid, ean, quantity, order_reference, malmberg_reference))

    def placeOrder3(self, order):
        return self.invoke(self._soap.service.RegisterOrder, (order.distributeur.id, order.artikel.ean, order.aantal, order.referentie, order.malmberg_referentie))

    def placeOrder2(self, distrid, ean, quantity, order_reference, malmberg_reference):


        '''Functie om orders te plaatsen.'''

        # <xs:element name="RegisterOrderRequest">
        #     <xs:complexType>
        #         <xs:sequence>
        #             <xs:element name="DistributorId" type="xs:string"/>
        #             <xs:element name="EAN" type="xs:string"/>
        #             <xs:element name="Quantity" type="xs:int"/>
        #             <xs:element name="OrderReference" type="xs:string"/>
        #             <xs:element name="MalmbergOrderReference" type="xs:string"/>
        #         </xs:sequence>
        #     </xs:complexType>
        # </xs:element>
        result ='?'
        try:
            response = self._soap.service.RegisterOrder(distrid, ean, quantity, order_reference, malmberg_reference)
            if response[0] == 500:
                result = response[1][0]['message']
            else:
                result = `response`
        except Exception, e:
            result = str(e)

        return result

class SpecificationService(SoapService):

    def __init__(self,host, distributorid='', password=''):
        SoapService.__init__(self, join_url_parts((host,'backend/ws/PublisherSpecificationService_v1_1?wsdl')) )
        # SoapService.__init__(self, join_url_parts((host,'ws/DistributorService?wsdl')) )

        # authheader = self._soap.factory.create('{urn:edupoort:publisher:specificationservice:types:1.1}AuthenticationHeader')
        # authheader.DistributorId = distributor
        # authheader.Password = password
        # namespace = ('prefix', 'urn:edupoort:publisher:specificationservice:types:1.0')
        # # namespace = ('urn', "urn:edupoort:publisher:specificationservice:types:1.0")
        # authheader = Element('AuthenticationHeader', ns=namespace)
        # # authheader = Element('AuthenticationHeader')
        # duid = Element('DistributorId').setText(distributor)
        # pwd = Element('Password').setText(password)
        # authheader.append(duid)
        # authheader.append(pwd)
        # self._soap.set_options(soapheaders=(authheader))
        self.distributorid = distributorid
        self.distributorpassword = password


    def SpecifyEBFCredit(self, EAN, StartDate, DistributorCreditId, DistributorUserId, DistributorSchoolId):
        '''
        SpecifyEBFCredit(xs:string EAN, xs:date StartDate, xs:string DistributorCreditId, xs:string DistributorUserId, xs:string DistributorSchoolId )
        '''
        result ='?'
        try:
            response = self._soap.service.SpecifyEBFCredit(EAN, xs_date(StartDate), DistributorCreditId, DistributorUserId, DistributorSchoolId)
            if response[0] == 500:
                result = "error " + `response[1][0]['Code']` + ": " + response[1][0]['Message']
            else:
                result = `response`
        except Exception, e:
            result = str(e)

        return result


    def SpecifyIBFCredit(self, EAN, Amount, StartDate, ContactName, ContactEmail, DistributorCreditId, DistributorSchoolId ):
        '''
        SpecifyIBFCredit(xs:string EAN, xs:int Amount, xs:date StartDate, ns2:Contact Contact, xs:string DistributorCreditId, xs:string DistributorSchoolId, )
        '''
        service = self._soap.service.SpecifyIBFCredit
        namespace = service.method.soap.input.body.parts[0].element[1]
        Contact = self._soap.factory.create('{%s}Contact' % (namespace))
        Contact.Name = ContactName
        Contact.Email = ContactEmail
        return self.invoke(service, (EAN, Amount, xs_date(StartDate), Contact, DistributorCreditId, DistributorSchoolId))

    def SpecifyIBFCredit2(self, specificatie, distributeur ):
        '''
        SpecifyIBFCredit(xs:string EAN, xs:int Amount, xs:date StartDate, ns2:Contact Contact, xs:string DistributorCreditId, xs:string DistributorSchoolId, )
        '''

        self.distributorid = distributeur.id
        self.distributorpassword = distributeur.wachtwoord

        service = self._soap.service.SpecifyIBFCredit
        namespace = service.method.soap.input.body.parts[0].element[1]
        Contact = self._soap.factory.create('{%s}Contact' % (namespace))
        Contact.Name = specificatie.contact_naam
        Contact.Email = specificatie.contact_email

        return self.invoke(service, (specificatie.artikel.ean, specificatie.aantal, xs_date(specificatie.start_datum), Contact,specificatie.distributeur_credit_id, specificatie.distributeur_school_id))
