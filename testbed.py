from dtdl.fixtures.googledrive import GoogleSpreadsheet
from dtdl.soap import *
import logging, re, time
import unittest

DATE_RE = re.compile("(dat(e|um))",re.IGNORECASE)
NUMBER_RE = re.compile("\d+\.?")
UNWANTED_RE = re.compile("[ /:.!()]")

def _method_name(str):
    parts = str.split(' ')
    if NUMBER_RE.match(parts[0]):
        parts = parts[1:]

    return ''.join(parts)

class TestServices(unittest.TestCase):


    def setUp(self):
        self.specify = SpecificationService('http://dtdl.tst.toegang.malmberg.nl/backend/','1','test')
        self.services = {
            'SpecifyEBFCredit':   ['EAN:','Start Date:','Credit Id:', 'User Id:', 'School Id:'],
        }
        doc = GoogleSpreadsheet('woensdag1528@gmail.com', 'Woensdag1459')
        self.worksheet = doc.get_sheet('Service requests', 'Specificeren')


    def testSheets(self):
        failed = []
        succeeded = 0

        if self.worksheet:
            rows = self.worksheet.get_rows()
            update_row = rows[0]
            current_onderdeel = None
            current_values = None
            servicecall = None
            scenario_name = None

            for row in rows:
                if row['onderdeel']:
                    method_name = _method_name(row['onderdeel'])

                    if method_name in self.services:
                        parameters = self.services[method_name]
                        servicecall = getattr(self.specify, method_name)

                if servicecall:
                    if row['scenario']:
                        if current_values:
                            values=[]
                            for p in parameters:
                                values.append(current_values[p])
                            print values
                            response = servicecall(*values)
                            update_row['resultaat'] = response
                            if re.sub(UNWANTED_RE,'',response).upper() == re.sub(UNWANTED_RE,'',update_row['verwachtresultaat']).upper():
                                akkoord = 'V'
                                succeeded += 1
                            else:
                                akkoord = 'x'
                                failed.append( "%s: %s"  % (method_name, scenario_name) )
                            update_row['akkoord'] = akkoord
                            self.worksheet.update_row(update_row)

                        scenario_name = row['scenario']
                        current_values = {}
                        update_row = row

                    veldnaam = row['velden']
                    cont=row['data']
                    if cont == 'huidige datum':
                        cont = datetime.now().date()
                    elif cont == '-':
                        cont = ''
                    if DATE_RE.search(veldnaam) and cont.__class__ is str:
                        if len(cont) > 0:
                            cont = datetime.strptime( cont, '%d-%m-%Y' ).date()

                    current_values[veldnaam] = cont

        print "Result=%s" % '\n'.join(failed)
        self.assertEqual( len(failed), 0, ("%d scenario's mislukt\n" % (len(failed))) + "\n".join(failed))
        self.assertGreater( succeeded, 0, 'Geen scenario\'s gevonden!')
